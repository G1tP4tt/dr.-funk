package Aktien;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public abstract class AbstractAssetValueCalculator {

	protected Map<String, Integer> stockQuantitiyByTickerSymbol;
	
	public AbstractAssetValueCalculator(String fileName) throws IOException {
		
		stockQuantitiyByTickerSymbol = new HashMap<String, Integer>();
		readPortfolio(fileName);
	}
	
	public abstract double getTotalValue()
		throws IOException, InterruptedException,ExecutionException;
	
	private void readPortfolio(String fileName) throws IOException {
		
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		String stockInfo = reader.readLine();
		while(stockInfo != null) {
			
			String[] items = stockInfo.split(",");
			String tickerSymbol = items[0];
			Integer quantity = Integer.valueOf(items[1]);
			stockQuantitiyByTickerSymbol.put(tickerSymbol, quantity);
			
			stockInfo = reader.readLine();
		}
		reader.close();
	}


}
