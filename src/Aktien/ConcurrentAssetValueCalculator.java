package Aktien;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ConcurrentAssetValueCalculator extends AbstractAssetValueCalculator {
	private final static double BLOCKING_COEFFICIENT = 0.95;

	public ConcurrentAssetValueCalculator(String fileName) throws IOException {
		super(fileName);
	}

	@Override
	public double getTotalValue() throws IOException, InterruptedException, ExecutionException {
		
		int numberOfCores = Runtime.getRuntime().availableProcessors();
		int poolSize = (int) (numberOfCores / (1 - BLOCKING_COEFFICIENT));

		System.out.println("Number of available cores = " + numberOfCores);
		System.out.println("Thread pool size= " + poolSize);

		List<Callable<Double>> tasks = new ArrayList<Callable<Double>>();

		for (String tickerSymbol : stockQuantitiyByTickerSymbol.keySet()) {
			tasks.add(new Callable<Double>() {
				
				@Override
				public Double call() throws IOException {
					int quantity = stockQuantitiyByTickerSymbol.get(tickerSymbol);
					return YahooFinance.getPrice(tickerSymbol) * quantity;

				}

			});
		}
		
		ExecutorService executionPool = Executors.newFixedThreadPool(poolSize);
		
		List<Future<Double>> prices =
				executionPool.invokeAll(tasks, 100, TimeUnit.SECONDS);
		
		
		double totalValue = 0;
		for (Future<Double> price : prices) {
			totalValue += price.get();
		}
		
		executionPool.shutdown();
		return totalValue;
		
		
	}

}
