package counter;

public class CounterTask extends Thread {
	
	private Sequence sequence;
	private int numValues;
	
	public CounterTask(Sequence sequence, int numValues) {
		
		this.sequence = sequence ;
		this.numValues = numValues;	
	}
	
	@Override
	public void run() {
		
		for (int i = 0; i < numValues; i++) {
			sequence.getAndIncrement();
		}
	}

}
