package counterDeadlock;

import java.util.List;

public class CounterTask extends Thread {

  private List<Sequence> lstSeq;
  private int numValues;

  public CounterTask(List<Sequence> lstSeq, int numValues) {

    this.lstSeq = lstSeq;
    this.numValues = numValues;
  }

  @Override
  public void run() {

    for (int i = 0; i < numValues; i++) {

      System.out.println(lstSeq.get(0).getName() + ", "
          + lstSeq.get(1).getName() + ": i = " + i);
      
      lstSeq.get(0).getAndIncrement(lstSeq.get(1));
    }
  }
}
