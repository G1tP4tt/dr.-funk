package hollywood;

import akka.actor.UntypedActor;

public class HollywoodActor extends UntypedActor {

	private String name;

	HollywoodActor(String name) {
		this.name = name;
	}

	@Override
	public void onReceive(Object role) throws Exception {
		if (role instanceof String) {
			System.out.println(name + " is playing role " + role + " from thread " + Thread.currentThread().getName());
		} else {
			System.out.println(name + " doesn't play a role from thread " + Thread.currentThread().getName());
		}
	}

}
