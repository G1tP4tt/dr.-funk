package oddEven;


public class OddNumberPrinter extends AbstractNumberPrinter {
	
	
    OddNumberPrinter(int limit, Object lockme){
        super(limit, lockme);
    }
    
    
    @Override protected boolean numberAccepted(int i){

        return (i % 2) > 0;
    }
}
