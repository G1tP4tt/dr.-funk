package primeCounter;

import java.util.concurrent.ExecutionException;

public abstract class AbstractPrimeFinder {

	public abstract int getPrimesInRange(int lower, int upper)
			throws InterruptedException, ExecutionException ;
	
	protected int countPrimesInRange(int lower, int upper) {
		
		int primeCount = 0;
		for (int i = lower; i <= upper; i++) {
			if(isPrime(i)) {
				
				primeCount++;
			}
		}
		
		return primeCount;
	}
	
	private boolean isPrime(int number) {
		if (number <= 1){
			return false;
		}
		
		for (int i = 2; i <= Math.sqrt(number); i++) {
			if (number % i == 0 ) {
				return false;
			}
		}
		
		return true;
	}
}
