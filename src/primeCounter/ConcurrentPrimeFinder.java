package primeCounter;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ConcurrentPrimeFinder extends AbstractPrimeFinder {

	private int poolSize;
	private int numberOfPartitions;

	public ConcurrentPrimeFinder(int poolSize, int NumberOfPartitions) {
		super();
		this.poolSize = poolSize;
		this.numberOfPartitions = NumberOfPartitions;
	}

	@Override
	public int getPrimesInRange(int lower, int upper) throws InterruptedException, ExecutionException {

		int primeCount = 0;
		List<Callable<Integer>> partitions = new ArrayList<Callable<Integer>>();
		int partitionSize = (upper - lower + 1) / numberOfPartitions;

		for (int i = 0; i < numberOfPartitions; i++) {
			int lowerBorder = i * partitionSize + 1;
			int upperBorder;

			if (i == numberOfPartitions - 1) {
				upperBorder = upper;

			} else {
				upperBorder = lowerBorder + partitionSize - 1;
			}

			partitions.add(new Callable<Integer>() {

				@Override
				public Integer call() throws Exception {
					return countPrimesInRange(lowerBorder, upperBorder);
				}
			});
			
		}

		ExecutorService executionPool = Executors.newFixedThreadPool(poolSize);

		List<Future<Integer>> countsInRange = executionPool.invokeAll(partitions, 10, TimeUnit.SECONDS);
		executionPool.shutdown();

		for (Future<Integer> count : countsInRange) {

			primeCount += count.get().intValue();

		}
		return primeCount;
	}
}
