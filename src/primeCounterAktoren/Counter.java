package primeCounterAktoren;

import java.util.concurrent.ExecutionException;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.Actors;
import akka.actor.UntypedActorFactory;
import akka.dispatch.Future;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.*;


public class Counter {

	private final static int initLOWER = 1;
	private final static int UPPER = 100;
	private final static int ANZAHL_BEREICHE = 5;

	public static void main(String[] args) throws IOException {
		
		
		

		ActorRef[] aktoren = new ActorRef[ANZAHL_BEREICHE];
		List<Future<Object>> results = new ArrayList<>();
		

		int range = ((UPPER - initLOWER) / ANZAHL_BEREICHE);
System.out.println(range);
		int lower = initLOWER;
		int upper = range;
		int i = 0;

		while (i < ANZAHL_BEREICHE) {
			
			aktoren[i] = Actors.actorOf(new UntypedActorFactory() {
				@Override
				public Actor create() {
					return new CounterActor();
				}
			}).start();

			aktoren[i].sendOneWay(new Boundaries(lower, upper));

			results.add(aktoren[i].sendRequestReplyFuture("calculate"));

			lower = upper + 1;
			upper += range ;

			i++;
		}

		int sum  = 0;
		for (int j = 0; j < ANZAHL_BEREICHE; j++) {
			sum +=  (int) results.get(j).await().get();
			

		}
		System.out.println(sum);

	}
}