package primeCounterAktoren;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import akka.actor.Actor;
import akka.actor.UntypedActor;
import primeCounter.AbstractPrimeFinder;

public class CounterActor extends UntypedActor {

	public int lowerBorder;
	public int upperBorder;

	public CounterActor() {

	}

	public CounterActor(int lower, int upper) {

		this.lowerBorder = lower;
		this.upperBorder = upper;
	}

	@Override
	public void onReceive(Object obj) throws Exception {

		if (obj.getClass() == Boundaries.class) {
			Boundaries b = (Boundaries) obj;
			this.lowerBorder = b.lower;
			this.upperBorder = b.upper ;
			
		} else {
			getContext().replySafe(countPrimesInRange(lowerBorder, upperBorder));
		}
	}

	// Primzahlen finden und aufsummieren
	protected int countPrimesInRange(int lower, int upper) {

		int primeCount = 0;
		for (int i = lower; i <= upper; i++) {
			if (isPrime(i)) {
				primeCount++;
			}
		}

		return primeCount;
	}

	private boolean isPrime(int number) {
		if (number <= 1) {
			return false;
		}

		for (int i = 2; i <= Math.sqrt(number); i++) {
			if (number % i == 0) {
				return false;
			}
		}

		return true;
	}

}
